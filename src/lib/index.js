import canvasHelper from './canvas'

// JavaScript helper functions
let lib = {
  ...canvasHelper
}

// Merge helper functions into LibraryManager to allow
// src-language code (e.g. C/C++, Rust) to talk to JavaScript
mergeInto(LibraryManager.library, lib)
