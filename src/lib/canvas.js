// Helper for interacting with the HTML5 Canvas

let canvasHelper = {
  jsArc__postset: '_jsArc();', // Solution for closures
  jsArc: function (x, y, r, sAngle, eAngle) {
    let canvas = document.getElementById('canvas')
    let ctx = canvas.getContext('2d')

    _jsArc = function (x, y, r, sAngle, eAngle, counterclockwise) {
      ctx.beginPath()
      ctx.arc(x, y, r, sAngle, eAngle, counterclockwise || false)
      ctx.stroke()
    }
  }
}

export default canvasHelper
