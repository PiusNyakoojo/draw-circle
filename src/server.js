var path = require('path')
var express = require('express')

var server = express()
server.use(express.static(__dirname))

// Serve index.html
server.get('/*', function (req, res) {
  res.sendFile(path.resolve(__dirname, './index.html'))
})

// Listen to port 8080
var port = process.env.PORT || 8080
server.listen(port, function () {
  console.log('Server listening on port: ' + port)
})
