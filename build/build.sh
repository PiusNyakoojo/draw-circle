
# Clear /dist directory
rm -rf './dist/*'

# Copy index.html
cp './src/index.html' './dist/index.html'

# Copy server.js
cp './src/server.js' './dist/server.js'

# Compile lib from ES6 to ES5
mkdir './dist/lib'

webpack --config './build/webpack.config.js'

# Compile webassembly with exported functions
emcc './src/main.c' -s WASM=1 -o './dist/a.out.js' \
  --js-library './dist/lib/index.js' \
  -s EXPORTED_FUNCTIONS='["_main"]' \
  -s EXTRA_EXPORTED_RUNTIME_METHODS='["ccall", "cwrap"]'
