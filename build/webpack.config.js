var path = require('path')

module.exports = {
  entry: path.resolve(__dirname, '../src/lib/index.js'),
  output: {
    path: path.resolve(__dirname, '../dist/lib'),
    filename: 'index.js'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['es2017']
      }
    }]
  }
}
